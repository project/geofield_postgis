<?php

namespace Drupal\geofield\Plugin\GeofieldBackend;

use Drupal\geofield\Plugin\GeofieldBackendBase;

/**
 * Postgis backend for Geofield.
 *
 * @GeofieldBackend(
 *   id = "geofield_backend_postgis",
 *   admin_label = @Translation("PostGIS Backend")
 * )
 */

// @TODO: Document.

class GeofieldBackendPostgis extends GeofieldBackendBase {

  /**
   * {@inheritdoc}
   */
  public function schema() {
    return array(
      'type' => 'geometry',
      'size' => 'big',
      'not null' => FALSE,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function save($geometry) {
    $geom = \Drupal::service('geophp.geophp')->load($geometry);
    $unpacked = unpack('H*', $geom->out('ewkb'));
    return $unpacked[1];
  }

  /**
   * {@inheritdoc}
   */
  public function load($value) {
    return \Drupal::service('geophp.geophp')->load($value);
  }
}
